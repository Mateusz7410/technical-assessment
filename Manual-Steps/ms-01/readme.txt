************************************************************
 Deploy custom metadata to selected instance.
************************************************************
1. Login into salesforce instance as System Administrator user.
2. Go into Setup -> Company Information.
3. Ensure that 'Activate Multiple Currencies' is enabled. (My assumption that it is needed becouse of different currencies in Total Opportunity Value).
    - if not, please click 'Edit' and enable that option. 
4. Go into ant_deploy folder.
5. Open 'build.properties' file.
6. Fill 'sf.username' field with you username.
7. Fill 'sf.password' field with the plain text password followed by security token.
8. Field 'sf.serverurl' should be:
    - https://login.salesforce.com -> for production or developer or playground editions
    - https://test.salesforce.com -> for sandboxes.
9. Using Ant Migration Tool, go into terminal and type 'ant deploy'.
    (installation guide: https://developer.salesforce.com/docs/atlas.en-us.daas.meta/daas/forcemigrationtool_container_install.htm; 
    don't forget about 'ant-salesforce.jar' file which should be pasted into ant 'lib' folder).

    Inside 'codepkg' folder there are already all metadata files needed in this assessment.

************************************************************
 Insert example Business__c records.
************************************************************
1. Execute code from insertRecords.apex in Anonymous Apex in Developer Console or through WorkBench.

************************************************************
 Check components.
************************************************************
1. Search for 'Business Tab' tab.
2. Examine 'User Statistics' and 'Business Sales Method' named components.
