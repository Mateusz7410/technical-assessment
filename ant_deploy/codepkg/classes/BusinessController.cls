public with sharing class BusinessController {

    @AuraEnabled(cacheable=true)
    public static List<BusinessWrapper> getBusinessInfoByRegions(String firstRegion, String secondRegion) {
        if (String.isBlank(firstRegion) || String.isBlank(secondRegion)) {
            return null;
        }
        List<BusinessWrapper> result = new List<BusinessWrapper>();
        String bothRegions = firstRegion + ';' + secondRegion;
        String bothRegionsReversed = secondRegion + ';' + firstRegion;
        Map<String, List<Business__c>> operatingRegionToBusiness = new Map<String, List<Business__c>>{
            firstRegion => new List<Business__c>(),
            secondRegion => new List<Business__c>(),
            bothRegions => new List<Business__c>()
        };
        for (Business__c b : [SELECT Id, Operating_Region__c, Sales__c, Name FROM Business__c WHERE Operating_Region__c INCLUDES(:firstRegion, :secondRegion, :bothRegions, :bothRegionsReversed)]) {
            String operatingRegion = b.Operating_Region__c;
            if (b.Operating_Region__c == bothRegionsReversed) {
                operatingRegion = bothRegions;
            }
            if (operatingRegionToBusiness.get(operatingRegion) != null) {
                operatingRegionToBusiness.get(operatingRegion).add(b);
            }
        }
        for (String operatingRegion : operatingRegionToBusiness.keySet()) {
            if (!operatingRegionToBusiness.get(operatingRegion).isEmpty()) {
                result.add(new BusinessWrapper(operatingRegionToBusiness.get(operatingRegion), operatingRegion.replaceAll(';', ', ')));
            }
        }
        return result;
    }

    public class BusinessWrapper {
        @AuraEnabled public List<Business__c> business { get; set; }
        @AuraEnabled public Double sumOfSales { get; set; }
        @AuraEnabled public String operatingRegion { get; set; }

        BusinessWrapper(List<Business__c> business, String operatingRegion) {
            this.business = business;
            this.operatingRegion = operatingRegion;
            Double sum = 0;
            for (Business__c b : business) {
                sum += b.Sales__c;
            }
            this.sumOfSales = sum;
        }
    }
}