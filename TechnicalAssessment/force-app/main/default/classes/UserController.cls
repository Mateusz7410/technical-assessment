public with sharing class UserController {

    @AuraEnabled(cacheable=true)
    public static List<UserStatsWrapper> getUserStats() {
        Map<Id, UserStatsWrapper> ownerIdToUserStatsWrapper = new Map<Id, UserStatsWrapper>();

        for (AggregateResult ar : [SELECT OwnerId, Owner.Name, COUNT(Id) FROM Lead GROUP BY OwnerId, Owner.Name]) {
            Id ownerId = (Id) ar.get('OwnerId');
            if (!ownerIdToUserStatsWrapper.containsKey(ownerId)) {
                ownerIdToUserStatsWrapper.put(ownerId, new UserStatsWrapper());
            }
            ownerIdToUserStatsWrapper.get(ownerId).ownerId = ownerId;
            ownerIdToUserStatsWrapper.get(ownerId).ownerName = (String) ar.get('Name');
            ownerIdToUserStatsWrapper.get(ownerId).totalLeads = String.valueOf((Integer) ar.get('expr0'));
        }

        List<Datetime> latestOpportunityDate = new List<Datetime>();
        for (AggregateResult ar : [SELECT OwnerId, Owner.Name, COUNT(Id), MAX(CreatedDate), SUM(Amount) FROM Opportunity GROUP BY OwnerId, Owner.Name]) {
            Id ownerId = (Id) ar.get('OwnerId');
            if (!ownerIdToUserStatsWrapper.containsKey(ownerId)) {
                ownerIdToUserStatsWrapper.put(ownerId, new UserStatsWrapper());
                ownerIdToUserStatsWrapper.get(ownerId).ownerId = ownerId;
            	ownerIdToUserStatsWrapper.get(ownerId).ownerName = (String) ar.get('Name');
            }
            ownerIdToUserStatsWrapper.get(ownerId).totalOpportunities = String.valueOf((Integer) ar.get('expr0'));
            ownerIdToUserStatsWrapper.get(ownerId).latestCreatedDate = ((Datetime) ar.get('expr1')).format('dd.mm.yyyy');
            ownerIdToUserStatsWrapper.get(ownerId).totalVal = String.valueOf(((Decimal) ar.get('expr2')));
            latestOpportunityDate.add((Datetime) ar.get('expr1'));
        }

        //check if salesforce instance has Multiple Currency enabled
        if (Schema.getGlobalDescribe().containsKey('CurrencyType')) {
            Set<Id> processedUsers = new Set<Id>();
            for (Opportunity opp : [SELECT OwnerId, CurrencyIsoCode, CreatedDate FROM Opportunity WHERE CreatedDate IN :latestOpportunityDate]) {
                if (!processedUsers.contains(opp.OwnerId)) {
                    ownerIdToUserStatsWrapper.get(opp.OwnerId).totalVal = opp.CurrencyIsoCode + ' ' + ownerIdToUserStatsWrapper.get(opp.OwnerId).totalVal;
                    processedUsers.add(opp.OwnerId);
                }
            }
        }

        return ownerIdToUserStatsWrapper.values();
    }

    public class UserStatsWrapper {
        @AuraEnabled public Id ownerId { get; set; }
        @AuraEnabled public String ownerName { get; set; }
        @AuraEnabled public String totalLeads { get {
            if (totalLeads == null) {
                totalLeads = '0';
            }
            return totalLeads;
        } set; }
        @AuraEnabled public String totalOpportunities { get {
            if (totalOpportunities == null) {
                totalOpportunities = '0';
            }
            return totalOpportunities;
        } set; }
        @AuraEnabled public String latestCreatedDate { get; set; }
        @AuraEnabled public String totalVal { get; set; }
    }
}