import { LightningElement, track, wire} from 'lwc';
import getUserStats from '@salesforce/apex/UserController.getUserStats';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Owner Id', fieldName: 'ownerId' },
    { label: 'Owner Name', fieldName: 'ownerName'},
    { label: 'Total Leads', fieldName: 'totalLeads'},
    { label: 'Total Opportunities', fieldName: 'totalOpportunities'},
    { label: 'Latest Created Date (Opp)', fieldName: 'latestCreatedDate'},
    { label: 'Total Val (Opp)', fieldName: 'totalVal'},
];

export default class BusinessSales extends LightningElement {
    @track columns = columns;
    @track data = [];

    @wire(getUserStats, {})
    loadUserStats(result) {
        if (result.error) {
            this.dispatchEvent(
                new ShowToastEvent({
                    message: (((result.error) && (result.error.body.message)) ? result.error.body.message : result.error),
                    variant: 'error',
                    mode: 'pester'
                })
            );
        }
        else if (result.data) {
            console.log(result.data);
            this.data = result.data;
        }
    }
}