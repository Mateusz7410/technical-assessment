import { LightningElement, track, wire} from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import BUSINESS_OBJECT from '@salesforce/schema/Business__c';
import OPERATING_REGION_FIELD from '@salesforce/schema/Business__c.Operating_Region__c';
import getBusinessInfoByRegions from '@salesforce/apex/BusinessController.getBusinessInfoByRegions';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class BusinessSales extends LightningElement {
    @track firstValue;
    @track secondValue;
    @track allOperatingRegionOptions;
    @track firstOperatingRegionOptions;
    @track secondOperatingRegionOptions
    @track dataToDisplay
    @track noDataToDisplay

    @wire(getBusinessInfoByRegions, {firstRegion: '$firstValue', secondRegion: '$secondValue'})
    loadBusinessInfo(result) {
        if (result.error) {
            this.dispatchEvent(
                new ShowToastEvent({
                    message: (((result.error) && (result.error.body.message)) ? result.error.body.message : result.error),
                    variant: 'error',
                    mode: 'pester'
                })
            );
            this.dataToDisplay = undefined
            if (this.firstValue && this.secondValue) {
                this.noDataToDisplay = true
            }
        }
        else if (result.data) {
            if (Array.isArray(result.data) && result.data.length) {
                var businessWrappers = JSON.parse(JSON.stringify(result.data))
                this.dataToDisplay = businessWrappers.map(businessWrapper => { businessWrapper.businessLen = businessWrapper.business.length; businessWrapper.business[0].isDisplayed = true; return businessWrapper; })
                this.noDataToDisplay = false
            } else {
                this.dataToDisplay = undefined
                if (this.firstValue && this.secondValue) {
                    this.noDataToDisplay = true
                }
            }
        }
    }

    @wire(getObjectInfo, { objectApiName: BUSINESS_OBJECT })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: OPERATING_REGION_FIELD})
    setAllOperatingRegionOptions({ data, error }) {
        if (data) {
            this.allOperatingRegionOptions = data.values;
            this.firstOperatingRegionOptions = data.values;
            this.secondOperatingRegionOptions = data.values;
        } 
    }

    handleChangeOfFirstPicklist(event) {
        this.firstValue = event.detail.value;
        this.secondOperatingRegionOptions = this.allOperatingRegionOptions.filter(opt => opt.value !== this.firstValue );
    }

    handleChangeOfSecondPicklist(event) {
        this.secondValue = event.detail.value;
        this.firstOperatingRegionOptions = this.allOperatingRegionOptions.filter(opt => opt.value !== this.secondValue );
    }
}