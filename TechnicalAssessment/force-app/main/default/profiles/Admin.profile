<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <tabVisibilities>
        <tab>Business_Tab</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>true</modifyAllRecords>
        <object>Business__c</object>
        <viewAllRecords>true</viewAllRecords>
    </objectPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Business__c.Operating_Region__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Business__c.Sales__c</field>
        <readable>true</readable>
    </fieldPermissions>
</Profile>