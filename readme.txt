To examine components you can either:
 - login into mine instance where it is prepared in the main App.
	- username: guest.user@brave-badger-w0j12g.com
	- password: send separately.
 - Deploy it to selected instance:
	- Please go to the 'Manual-Steps' folder -> ms-01 -> readme.txt.


All developed metadata and classes are inside 'TechnicalAssessment' and 'ant_deploy' folders.

This whole zip is also a git repository available at: https://gitlab.com/Mateusz7410/technical-assessment.

In case of any issues please contact me: Mateusz7410@gmail.com.